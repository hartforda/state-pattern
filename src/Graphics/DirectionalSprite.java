package Graphics;

import Engine.KeyInput;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/**
 * A sprite that has different images for each direction
 * @author Connor Hibbs
 */
public class DirectionalSprite extends Sprite{
    
    /** The current direction the sprite is facing */
    private Direction currentDirection;
    
    /** The sprites to show for each direction */
    private Map<Direction, Sprite> directions;
    
    /** The different directions */
    public enum Direction {
        LEFT, RIGHT, FORWARD, BACKWARD
    }
    
    /**
     * Constructor - creates an empty DirectionalSprite
     */
    public DirectionalSprite(){
        directions = new HashMap<>();
    }
    
    /**
     * Adds a direction to this sprite
     * @param sprite the sprite to add
     * @param direction the direction this sprite should be shown for
     */
    public void addDirection(Sprite sprite, Direction direction){
        directions.put(direction, sprite);
    }
    
    /**
     * Updates the individual sprites for each direction if they are animations
     */
    @Override
    public void tick(){
        if(KeyInput.left()){
            currentDirection = Direction.LEFT;
        } else if(KeyInput.right()){
            currentDirection = Direction.RIGHT;
        } else {
            currentDirection = Direction.FORWARD;
        }
        
        //tick all of the sprites
        for(Map.Entry entry : directions.entrySet()){
            ( (Sprite)entry.getValue() ).tick();
        }
    }
    
    /**
     * @return the correct sprite for the current direction
     */
    @Override
    public BufferedImage getImage() {
        return directions.get(currentDirection).getImage();
    }
}
