package Graphics;

import java.awt.image.BufferedImage;
import Graphics.DirectionalSprite.Direction;

/**
 * By keeping track of all images in a single class, there is only one copy of each image loaded into
 * the program, which reduced memory usage
 */
public class Assets {
    
    public static BufferedImage[] coins, lives;

    public static BufferedImage gameOver, playAgainL, playAgainW, youWon;

    public static BufferedImage background;
    
    public static StaticSprite wall, spike;
    
    public static Animation fire, coin;
    
    public static DirectionalSprite player;
    
    /**
     * Loads in all of the images into the program
     */
    public static void init(){
        background = SpriteManager.loadImage("backgrounds/space_background.png");
        
        wall = SpriteManager.loadSprite("sprites/wall.jpg");
        spike = SpriteManager.loadSprite("sprites/spike.png");
    
        fire = SpriteManager.loadAnimation("sprites/fire.gif", 32, 32, 10);
        coin = SpriteManager.loadAnimation("sprites/coin3.gif", 32, 32, 10);
    
        Animation playerAnimation = SpriteManager.loadAnimation("sprites/player.gif", 32, 32, 100);
        player = SpriteManager.convertToDirectional(playerAnimation,
                Direction.FORWARD, Direction.RIGHT, Direction.LEFT, Direction.BACKWARD);
    
        //Screen Overlays
        coins = new BufferedImage[11];
        for(int i = 0; i <= 10; i++){
            coins[i] = SpriteManager.loadImage("overlay/" + i + "coins.png");
        }
    
        lives = new BufferedImage[4];
        for(int i = 0; i <=3; i++){
            lives[i] = SpriteManager.loadImage("overlay/" + i + "lives.png");
        }

        gameOver = SpriteManager.loadImage("overlay/gameover.png");
        youWon = SpriteManager.loadImage("overlay/youwon.png");
        playAgainW = SpriteManager.loadImage("overlay/playagainw.png");
        playAgainL = SpriteManager.loadImage("overlay/playagainl.png");
    }
}
