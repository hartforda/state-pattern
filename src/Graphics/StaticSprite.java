package Graphics;

import java.awt.image.BufferedImage;

/**
 * Class to hold background tiles. Has basic collision ability with
 * isSolid() method. Override in extended tile to allow collision testing
 * @author Connor Hibbs
 */
public class StaticSprite extends Sprite{
    
    /** The image to show */
    protected BufferedImage image;
    
    /**
     * Constructor - creates the Sprite and saves the image
     * @param image image to show when rendering sprite
     */
    public StaticSprite(BufferedImage image){
        this.image = image;
    }
    
    /**
     * @return the only image for the Sprite
     */
    public BufferedImage getImage(){
        return image;
    }
}
