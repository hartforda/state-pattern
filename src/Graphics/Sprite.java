package Graphics;

import Engine.Controller;

import java.awt.image.BufferedImage;

/**
 * Abstract representation of a graphic for an Entity
 * @author Connor Hibbs
 */
public abstract class Sprite {
    
    /** Width and height of the Sprite */
    protected int width, height;
    
    /**
     * Constructor - creates a sprite with a specific width and height
     * @param width width of the Sprite
     * @param height height of the Sprite
     */
    public Sprite(int width, int height){
        this.width = width;
        this.height = height;
    }
    
    /**
     * Constructor - creates a sprite with the default grid width and height
     */
    public Sprite() {
        int grid = Controller.GRID_SIZE;
        this.width = grid;
        this.height = grid;
    }
    
    /**
     * Abstract method requiring all Sprites to provide an image
     * @return the image that should be rendered for the sprite
     */
    public abstract BufferedImage getImage();
    
    /**
     * Updates the sprite if necessary every 60th of a second
     */
    public void tick(){
        
    }
    
}
