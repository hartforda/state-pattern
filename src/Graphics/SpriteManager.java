package Graphics;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.InputMismatchException;

import Graphics.DirectionalSprite.Direction;

/**
 * Utility class to load in sprites from files
 * @author Connor Hibbs
 */
public class SpriteManager {
    
    /**
     * Loads in a static image from a file
     * @param path path to the file (don't forget the folder)
     * @return BufferedImage sprite
     */
    public static BufferedImage loadImage(String path){
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File("res/" + path));
        } catch (IOException e) {
            System.out.println("Error loading image from " + path);
            e.printStackTrace();
            System.exit(1);
        }
        if(image == null) System.err.println("Image was null");
        return image;
    }
    
    public static StaticSprite loadSprite(String path){
        return new StaticSprite(loadImage(path));
    }
    
    
    public static Animation loadAnimation(String path, int width, int height, int speed){
        path = path.toLowerCase();
        String[] parts = path.split("\\.");
        if(parts[1].equals("gif")){
            return loadGIFAnimation(path, speed);
        } else {
            return loadSpriteSheetAnimation(path, width, height, speed);
        }
    }
    
    public static DirectionalSprite convertToDirectional(Animation anim, Direction... order){
        BufferedImage[] frames = anim.getFrames();
        int length = frames.length;
        int iterations = order.length;
        int framesPerDirection = length / iterations;
        boolean isStatic = (framesPerDirection == 1);
    
        if(length % iterations != 0){
            throw new InputMismatchException("The animation length must be divisible by the number of directions");
        }
        
        DirectionalSprite directional = new DirectionalSprite();
        for(int i = 0; i < iterations; i++){
            BufferedImage[] images = new BufferedImage[framesPerDirection];
            for(int j = 0; j < framesPerDirection; j++){
                int frame = i*iterations + j;
                images[j] = frames[frame];
            }
            Sprite sprite = isStatic ? new StaticSprite(images[0]) : new Animation(anim.getSpeed(), images);
            directional.addDirection(sprite, order[i]);
        }
        return directional;
    }
    
    /**
     * Loads an animation from a sprite sheet from top -> bottom, right -> left
     * @param path the path to the sprite sheet
     * @param width the width of the picture (in sub pictures)
     * @param height the height of the picture (in sub pictures)
     * @return BufferedImage[] containing all of the images in order
     */
    private static Animation loadSpriteSheetAnimation(String path, int width, int height, int speed){
        SpriteSheet sheet = new SpriteSheet(path, width, height);
        BufferedImage[] animation = new BufferedImage[width*height];
    
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                animation[y*height + x] = sheet.crop(x, y);
            }
        }
        return new Animation(speed, animation);
    }
    
    private static Animation loadGIFAnimation(String path, int speed){
        BufferedImage[] animation = null;
        try {
            InputStream stream = new FileInputStream(new File("res/" + path));
            ImageReader reader = (ImageReader) ImageIO.getImageReadersByFormatName("gif").next();
            reader.setInput(ImageIO.createImageInputStream(stream));
            
            int numImages = reader.getNumImages(true);
            
            animation = new BufferedImage[numImages];
            
            for(int i = 0; i < numImages; i++){
//                BufferedImage image = reader.read(i);
                animation[i] = reader.read(i);
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return new Animation(speed, animation);
    }
    
}
