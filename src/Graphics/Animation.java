package Graphics;

import java.awt.image.BufferedImage;

/**
 * A type of Sprite that has an array of images which are looped through creating an animated Sprite
 */
public class Animation extends Sprite{

    /** the number of milliseconds for each image to last */
    private int speed;
    
    /** the current frame of the animation */
    private int index;
    
    /** The time that the last frame was drawn */
    private long lastTime;
    
    /** the time since the last frame */
    private long timer;
    
    /** The array of images to loop through */
    private BufferedImage[] frames;
    
    /**
     * Creates an animated sprite
     * @param speed Duration of a frame in milliseconds
     * @param frames The images to loop through
     */
    public Animation(int speed, BufferedImage[] frames){
        this.speed = speed;
        this.frames = frames;
        index = 0;
        lastTime = System.currentTimeMillis();
        timer = 0;//
    }
    
    /**
     * Updates the animation by checking the time, and if it is past when it should have
     * switched frames, the next frame is shown
     */
    public void tick(){
        timer += System.currentTimeMillis() - lastTime;
        lastTime = System.currentTimeMillis();

        if(timer > speed){
            index++;
            timer = 0;
            if(index >= frames.length){
                index = 0;
            }
        }
    }
    
    /**
     * @return the current frame of the animation
     */
    @Override
    public BufferedImage getImage(){
        return frames[index];
    }
    
    /**
     * @return the array of images used in the animation
     */
    public BufferedImage[] getFrames() {
        return frames;
    }
    
    /**
     * @return the duration of each frame in milliseconds
     */
    public int getSpeed() {
        return speed;
    }
}
