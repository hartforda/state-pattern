package Entity;

import Engine.Collisions;
import Engine.KeyInput;
import Engine.Vector;
import Graphics.Sprite;

import java.awt.*;

/**
 * A subclass of Entity that can move
 *
 * @author Connor Hibbs
 */
public abstract class DynamicEntity extends Entity {
    
    /**
     * Enable to make the Entity move with gravity
     */
    protected boolean useGravity = false;
    
    /**
     * Current getVelocity
     */
    protected Vector velocity;
    
    /**
     * Gravity to apply if gravity is enabled
     */
    protected double GRAVITY = 0.25;
    
    /** Speed to move at */
//    protected Vector speed;
    
    /**
     * Constructor - creates an Entity with an initial getVelocity
     *
     * @param position initial position
     * @param velocity initial getVelocity
     * @param sprite   image to be drawn
     */
    public DynamicEntity(Vector position, Vector velocity, Sprite sprite) {
        super(position, sprite);
        this.velocity = velocity;
//        speed = new Vector(getVelocity.x(), getVelocity.y());
    }
    
    /**
     * Constructor - creates an player with zero initial getVelocity
     *
     * @param position starting position
     * @param sprite   image to be drawn
     */
    public DynamicEntity(Vector position, Sprite sprite) {
        this(position, new Vector(0, 0), sprite);
    }
    
    /**
     * Called once every 60th of a second. Updates the collision bounds, getVelocity, and position
     */
    @Override
    public void tick() {
        super.tick();
        this.bounds = new Rectangle((int) position.x(), (int) position.y(), GRID_SIZE, GRID_SIZE);
        updateVelocity();
        move();
    }
    
    /**
     * Updates the getVelocity of an Entity, so the position can be changed if there is no collision
     */
    protected void updateVelocity() {

        applyGravity();
        
        //TODO this code has been moved to individual states
        //it is worth keeping around for now in case another entity is added that can move, but does
        // not need to use the state pattern
        
        
//        Vector future = Vector.add(position, new Vector(0, getVelocity.y()));
//        Rectangle futureRect = new Rectangle((int)future.x(), (int)future.y(), width, height);
//        boolean collision = Collisions.checkCollision(this, futureRect);
//
//        if(collision){
//            System.out.println("Colliding with something");
//            getVelocity.y(0);
//        }
//
//        else {
//            if(KeyInput.up()){
//                getVelocity.y(-speed.y());
//            } else if(KeyInput.down()){
//                getVelocity.y(speed.y());
//            } else {
//                getVelocity.y(0);
//            }
//        }
//
//        if(isSolid){
//            Vector future = Vector.add(position, new Vector(0, getVelocity.y()));
//            Rectangle futureRect = new Rectangle((int)future.x(), (int)future.y(), width, height);
//            boolean collision = Collisions.checkCollision(self, futureRect);
//
//            if(collision){
//                System.out.println("Colliding with something");
//                getVelocity.y(0);
//                if(KeyInput.up()){
//                    getVelocity.y(-speed.y());
//                }
//            }
//        } else {
//            if(KeyInput.up()){
//                System.out.println("Jumping...");
//                getVelocity.y(-speed.y());
//            }
//        }
//
//        if(KeyInput.left()){
//            getVelocity.x(-speed.x());
//        } else if(KeyInput.right()){
//            getVelocity.x(speed.x());
//        } else {
//            getVelocity.x(0);
//        }
    }
    
    /**
     * Factors gravity into the getVelocity of an entity
     */
    protected void applyGravity() {
        if (useGravity) {
            velocity.y(velocity.y() + GRAVITY); //add in the getVelocity automatically
        }
    }
    
    
    /**
     * Checks the desired getVelocity for collisions, and if none are found (or the object isn't solid)
     * it updates the position of the Entity
     */
    protected void move() {
        Vector future = Vector.add(position, new Vector(velocity.x(), 0));
        Rectangle futureRect = new Rectangle((int) future.x(), (int) future.y(), width, height);
        boolean collision = Collisions.checkCollision(self, futureRect);
    
        if (!isSolid || !collision/* && future.x() > 0 && future.x() < 990*/) {
            position.x(position.x() + velocity.x());
        }
    
        future = Vector.add(position, new Vector(0, velocity.y()));
        futureRect = new Rectangle((int) future.x(), (int) future.y(), width, height);
        collision = Collisions.checkCollision(self, futureRect);
    
        if (!isSolid || !collision/* && future.y() > 0 && future.y() < 990*/) {
            position.y(position.y() + velocity.y());
        } else {
            velocity.y(0);
        }
    }
    
    /**
     * @return Velocity Vector
     */
    public Vector getVelocity() {
        return velocity;
    }
}