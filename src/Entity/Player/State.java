package Entity.Player;

import Engine.Collisions;
import Engine.Vector;
import Entity.Entity;
import Entity.Hazards.Hazard;
import Entity.Items.Item;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * State.java
 * Defines basic behavior that a state would need to control
 * @author Connor Hibbs, Alex Hartford
 */
public abstract class State {
    
    /** allows some debugging messages to be printed out */
    protected static final boolean DEBUG = false;
    
    /** The player all the actions are being done on */
    protected Player player;
    
    /**
     * Constructor - creates the state and saves the player it is acting on
     * @param player the player which is in this state
     */
    public State(Player player){
        this.player = player;
    }
    
    /**
     * Updates the getVelocity of the Player
     */
    public void updateVelocity() {
        
    }
    
    /**
     * Handles player movement
     */
    public void move() {
        Vector future = Vector.add(player.getPosition(), new Vector(player.getVelocity().x(), 0));
        Rectangle futureRect = new Rectangle((int)future.x(), (int)future.y(), player.getWidth(), player.getHeight());
        boolean collision = Collisions.checkCollision(player, futureRect);
    
        if (!player.isSolid() || !collision/* && future.x() > 0 && future.x() < 990*/) {
            player.getPosition().x(player.getPosition().x() + player.getVelocity().x());
        }
    
        future = Vector.add(player.getPosition(), new Vector(0, player.getVelocity().y()));
        futureRect = new Rectangle((int)future.x(), (int)future.y(), player.getWidth(), player.getHeight());
        collision = Collisions.checkCollision(player, futureRect);
    
        if (!player.isSolid() || !collision/* && future.y() > 0 && future.y() < 990*/) {
            player.getPosition().y(player.getPosition().y() + player.getVelocity().y());
        } else {
            player.getVelocity().y(0);
        }
    }
    
    /**
     * Updates the State if the State has changed. By default,
     * the state checks to see if there are any coins the player has collided with
     */
    public void tick(){
        if(Collisions.checkCollision(player, Hazard.class)) player.loseLife();
    
        Entity other = Collisions.checkClassCollision(player, Item.class);
        if(other != null){
            player.gainCoin();
            other.destroy();
        }
    }

    /**
     * Overridden by child classes
     */
    public void start(){
        
    }

    /**
     * Overridden by child classes
     */
    public void finish(){
        
    }

    /**
     * Handles key input by user
     * @param keyCode - integer value representing the key pressed by the user
     */
    public void keyPressed(int keyCode){
        switch(keyCode) {
            case KeyEvent.VK_RIGHT:
                player.setState(new WalkingRightState(player));
                break;
            case KeyEvent.VK_LEFT:
                player.setState(new WalkingLeftState(player));
                break;
        }
    }

    /**
     * Overridden by child classes
     * @param keyCode - integer value representing the key pressed by the user
     */
    public void keyReleased(int keyCode){
        
    }
}
