package Entity.Player;

/**
 * WalkingState.java
 * Parent class for walking left and right
 * @author Connor Hibbs
 */
public abstract class WalkingState extends HorizontalState{

    /**
     * Constructor - calls super() and puts the player into a walking state
     * @param player - Player entering a walking state
     */
    public WalkingState(Player player){
        super(player);
        speed = 5;
    }
}
