package Entity.Player;

import Engine.Collisions;
import Engine.KeyInput;
import Engine.Vector;

import java.awt.*;

/**
 * JumpingState.java
 * Handles user jumping
 * @author Connor Hibbs, Alex Hartford
 */
public class JumpingState extends State {

    /**
     * Constructor - calls super() and puts the player into the jumping state
     * @param player - Player being put into jumping state
     */
    public JumpingState(Player player){
        super(player);
    }

    /**
     * Handles player collisions during jumping and assures the player remains moving in the same direction
     */
    public void start(){
        if(DEBUG) System.out.println("State = Jumping");
        if(!KeyInput.up()) {
            player.prevState();
            return;
        }
    
        Vector future = Vector.add(player.getPosition(), new Vector(0, player.getHeight()/2));
        Rectangle futureRect = new Rectangle((int)future.x(), (int)future.y(), player.getWidth(), player.getHeight());
        boolean collision = Collisions.checkCollision(player, futureRect);
        
        if(!collision){
            player.prevState();
            return;
        }
        player.getVelocity().y(-7.5);
    }

    /**
     * Handles collisions with objects during jumping
     */
    public void tick(){
        super.tick();
    
        Vector future = Vector.add(player.getPosition(), new Vector(0, player.getVelocity().y()));
        Rectangle futureRect = new Rectangle((int)future.x(), (int)future.y(), player.getWidth(), player.getHeight());
        boolean collision = Collisions.checkCollision(player, futureRect);
    
        if(collision){
            player.getVelocity().y(0);
            player.prevState();
        }
    }

    /**
     * Returns a String indicating the player is in the jumping state
     * @return String containing "Jumping State"
     */
    public String toString(){
        return "Jumping State";
    }
}
