package Entity.Player;

/**
 * RunningState.java
 * Parent class for running left and right
 * @author Connor Hibbs
 */
public abstract class RunningState extends HorizontalState {

    /**
     * Constructor, calls super() and puts the player into a running state
     * @param player - player entering the running state
     */
    public RunningState(Player player) {
        super(player);
        speed = 10;
    }
}
