package Entity.Player;

import Engine.KeyInput;

import java.awt.event.KeyEvent;

/**
 * WalkingLeftState.java
 * Indicates the player is in the walking left state
 * @author Connor Hibbs
 */
public class WalkingLeftState extends WalkingState {

    /**
     * Constructor - calls super()
     * @param player - player being put into the walking left state
     */
    public WalkingLeftState(Player player){
        super(player);
    }

    /**
     * Sets the current state to the previous state if the user releases the key
     */
    public void start(){
        if(DEBUG) System.out.println("State = Walking Left");
        if(!KeyInput.left()) {
            player.prevState(); //if the left key isn't still pressed, go back again
            return;
        }
        player.getVelocity().x(-speed);
    }

    /**
     * Calls super() and if the left key was released it will set the current state to the previous state
     * @param keyCode - integer value representing the key pressed by the user
     */
    public void keyReleased(int keyCode){
        super.keyReleased(keyCode);
        if(keyCode == KeyEvent.VK_LEFT){
            player.prevState();
        }
    }

    /**
     * Returns a String indicating the player is in the walking left state
     * @return String containing "Walking Left State"
     */
    public String toString(){
        return "Walking Left State";
    }
}
