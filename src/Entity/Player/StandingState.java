package Entity.Player;

import java.awt.event.KeyEvent;

/**
 * StandingState.java
 * Neutral movement state, no user input
 * @author Connor Hibbs
 */
public class StandingState extends HorizontalState {
    
    private long lastLeft = 0, lastRight = 0;

    /**
     * Constructor - Calls super() and sets player speed to 0
     * @param player - player in the standing state
     */
    public StandingState(Player player){
        super(player);
        speed = 0;
    }

    /**
     * Keeps the player's speed at 0 as long as it is in the standing state
     */
    public void start(){
        if(DEBUG) System.out.println("State = Standing");
        speed = 0;
        player.getVelocity().x(speed);
    }

    /**
     * Handles user key input
     * @param keyCode - integer value representing the key pressed by the user
     */
    public void keyPressed(int keyCode){
        super.keyPressed(keyCode);
        
        if(keyCode == KeyEvent.VK_LEFT) {
            if (System.currentTimeMillis() - lastLeft < DOUBLE_TAP_RATE) {
                player.setState(new RunningLeftState(player));
            }
            lastLeft = System.currentTimeMillis();
        } else if(keyCode == KeyEvent.VK_RIGHT){
            if(System.currentTimeMillis() - lastRight < DOUBLE_TAP_RATE){
                player.setState(new RunningRightState(player));
            }
            lastRight = System.currentTimeMillis();
        }
    }

    /**
     * Returns a String indicating the player is in the standing state
     * @return String containing "Standing State"
     */
    public String toString(){
        return "Standing State";
    }
}
