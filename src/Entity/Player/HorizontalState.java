package Entity.Player;

import java.awt.event.KeyEvent;

/**
 * HorizontalState.java
 * Parent class for walking, running, and standing
 * @author Connor Hibbs, Alex Hartford
 */
public abstract class HorizontalState extends State {
    
    /** The speed to move the player horizontally */
    protected double speed = 0;
    
    /** Max time between taps (ms) to advance the player to a running state */
    protected static final int DOUBLE_TAP_RATE = 175;

    /**
     * Constructor - calls super() and puts the player into a horizontal state
     * @param player - Player being put into the horizontal state
     */
    public HorizontalState(Player player){
        super(player);
    }

    /**
     * Handles key input by user
     * @param keyCode - integer value representing the key pressed by the user
     */
    public void keyPressed(int keyCode){
        super.keyPressed(keyCode);
        switch(keyCode){
            case KeyEvent.VK_UP:
                player.setState(new JumpingState(player));
                break;
        }
    }
}
