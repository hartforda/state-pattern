package Entity.Player;

import Engine.KeyInput;

import java.awt.event.KeyEvent;

/**
 * RunningRightState.java
 * Indicates the player is in the running right state
 * @author Connor Hibbs
 */
public class RunningRightState extends RunningState {

    /**
     * Constructor - calls super()
     * @param player - player entering the running right state
     */
    public RunningRightState(Player player){
        super(player);
    }

    /**
     * Sets the current state to the previous state if the user releases the key
     */
    public void start(){
        if(DEBUG) System.out.println("State = Running Right");
        if(!KeyInput.right()) {
            player.prevState(); //if right isn't still pressed, go back one more
            return;
        }
        player.getVelocity().x(speed);
    }

    /**
     * Calls super() and if the right key was released it will set the current state to the previous state
     * @param keyCode - integer value representing the key pressed by the user
     */
    public void keyReleased(int keyCode){
        super.keyReleased(keyCode);
        if(keyCode == KeyEvent.VK_RIGHT){
            player.prevState();
        }
    }

    /**
     * Returns a String indicating the player is in the running right state
     * @return String containing "Running Right"
     */
    public String toString(){
        return "Running Right";
    }
}
