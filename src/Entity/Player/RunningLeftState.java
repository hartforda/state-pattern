package Entity.Player;

import Engine.KeyInput;

import java.awt.event.KeyEvent;

/**
 * RunningLeftState.java
 * Indicates the player is in the running left state
 * @author Connor Hibbs
 */
public class RunningLeftState extends RunningState {

    /**
     * Constructor - calls super and puts the player into the running left state
     * @param player - player entering the running left state
     */
    public RunningLeftState(Player player){
        super(player);
    }

    /**
     * Sets the current state to the previous state if the user releases the key
     */
    public void start(){
        if(DEBUG) System.out.println("State = Running Left");
        if(!KeyInput.left()) {
            player.prevState(); //if the left key isn't still pressed, go back again
            return;
        }
        player.getVelocity().x(-speed);
    }

    /**
     * Calls super() and if the left key was released it will set the current state to the previous state
     * @param keyCode - integer value representing the key pressed by the user
     */
    public void keyReleased(int keyCode){
        super.keyReleased(keyCode);
        if(keyCode == KeyEvent.VK_LEFT){
            player.prevState();
        }
    }

    /**
     * Returns a String indicating the player is in the running left state
     * @return String containing "Running Left"
     */
    public String toString(){
        return "Running Left";
    }
}
