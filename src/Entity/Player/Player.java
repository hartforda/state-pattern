package Entity.Player;

import Engine.*;
import Entity.*;
import Graphics.Assets;
import StackVisualizer.StackApp;

import java.util.Stack;

/**
 * The player object that uses the state pattern to handle events
 * @author Connor Hibbs, Alex Hartford
 */
public class Player extends DynamicEntity {
    
    /**
     * Stack of states. Can add state
     */
    private Stack<State> states;
    
    /**
     * The current state of the Player Entity
     */
    private State state;

    /**
     * Player lives
     */
    private int lives;

    /**
     * Player coins
     */
    private int coins;

    /**
     * Instance of the stack viewing tool
     */
    private StackApp stackApp;
    
    /**
     * Constructor - creates a player at an initial position
     * @param position the initial position of the player
     */
    public Player(Vector position){
        super(position, Assets.player);
        useGravity = true;
        type = "Player";
        isSolid = true;
        lives = 3;
        coins = 0;

        //start the stack off with a standing state
        states = new Stack<>();
        state = new StandingState(this);
        states.push(state);
        
        Controller.getInstance().follow(this);
        KeyInput.getInstance().subscribe(this);
        
        stackApp = new StackApp();
    }

    /**
     * Removes a life from the player, updates the overlay, and places the player at its initial position
     */
    public void loseLife() {
        lives -= 1;
        Controller.getInstance().updateOverlay("life");
        setPosition(new Vector(anchor.x(), anchor.y()));
    }

    /**
     * Adds a coin to the player and updates the overlay
     */
    public void gainCoin() {
        coins += 1;
        Controller.getInstance().updateOverlay("coin");
    }
    
    /**
     * Handle all tick updates by doing default code in DynamicEntity,
     * then passing control to the states
     */
    public void tick(){
        super.tick();
        state.tick();
    }
    
    /**
     * Updates the getVelocity of the player by applying any special behavior in the current state
     */
    @Override
    public void updateVelocity(){
        applyGravity();
        state.updateVelocity();
    }
    
    /**
     * Moves the player by asking the state how it should move
     */
    @Override
    public void move(){
        state.move();
    }

    /**
     * Handles key press from the user
     * @param keyCode - integer value representing the key pressed
     */
    public void keyPressed(int keyCode){
        state.keyPressed(keyCode);
    }

    /**
     * Handles key release from the user
     * @param keyCode - integer value representing the key pressed
     */
    public void keyReleased(int keyCode){
        state.keyReleased(keyCode);
    }

    /**
     * Sets the state of the player
     * @param state - state to put the player in
     */
    public void setState(State state){
        this.state.finish();
        states.push(state); //add the next state to the stop
        this.state = state;
        this.state.start();
        
        stackApp.setState(state); //show the change in state on the stack visualizer
    }

    /**
     * Sets the current state to the previous state
     */
    public void prevState(){
        states.pop().finish();
        this.state = states.peek();
        this.state.start();
        
        stackApp.prevState(); //show the change in state on the stack visualizer
    }
}
