package Entity;

import Graphics.Assets;
import Engine.Vector;
import Graphics.StaticSprite;

/**
 * A wall to outline the course
 * @author Connor Hibbs
 */
public class Wall extends Entity {
    
    /**
     * Constructor - creates a solid wall
     * @param position the starting position
     */
    public Wall(Vector position){
        super(position, Assets.wall);
        isSolid = true;
        type = "wall";
    }
}
