package Entity.Hazards;

import Engine.Vector;
import Entity.Entity;
import Graphics.Sprite;

/**
 * Parent class for all hazards
 * @author Connor Hibbs
 */
public abstract class Hazard extends Entity {
    
    /**
     * Constructor - creates a hazard
     * @param position the position in the world
     * @param sprite the sprite to show
     */
    public Hazard(Vector position, Sprite sprite){
        super(position, sprite);
        type = "hazard";
    }
}
