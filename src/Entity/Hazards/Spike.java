package Entity.Hazards;

import Engine.Vector;
import Graphics.Assets;
import Graphics.StaticSprite;

/**
 * The spike entity. The player loses a life if he touches the spikes
 * @author Connor Hibbs
 */
public class Spike extends Hazard {
    
    /**
     * Constructor
     * @param position the position in the game
     */
    public Spike(Vector position){
        super(position, Assets.spike);
        
        isSolid = false; //true;
        type = "spike";
    }
}
