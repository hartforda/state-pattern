package Entity.Hazards;

import Engine.Vector;
import Graphics.Animation;
import Graphics.Assets;

/**
 * Fire entity. the player loses a life if it touches fire
 * @author Connor Hibbs
 */
public class Fire extends Hazard {
    
    /**
     * Constructor - creates the fire element
     * @param position the position in the world
     */
    public Fire(Vector position){
        super(position, Assets.fire);
        
        isSolid = false;
        type = "fire";
    }
}
