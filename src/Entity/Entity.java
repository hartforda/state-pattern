package Entity;

import Engine.Controller;
import Engine.Vector;
import Graphics.Sprite;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Entity and all of the information that an Entity needs to keep track of
 * Stationary entities can extend this class, but entities that can move should extend the
 * DynamicEntity class
 * @author Connor Hibbs
 */
public abstract class Entity {
    
    /** A reference to itself to be used in States*/
    protected final Entity self;
    
    /** Does the player collide with solid objects */
    protected boolean isSolid = false;
    
    /** Width and height of the Entity */
    protected int width = 32, height = 32;
    
    /** The image that should be drawn for the player */
    protected Sprite sprite;
    
    /** The initial position of the Entity */
    protected final Vector anchor;
    
    /** The current position of the Entity */
    protected Vector position;
    
    /** The collision bounds of the Entity */
    protected Rectangle bounds;
    
    /** The type of the Entity */
    protected String type = "";
    
    /** The grid size of the world */
    protected static final int GRID_SIZE = Controller.GRID_SIZE;
    
    /**
     * Constructor - creates an Entity at an initial position with an image
     * @param position The initial position of the Entity
     * @param sprite the image to be rendered when the Entity is drawn
     */
    public Entity(Vector position, Sprite sprite){
        this.self = this;
        this.position = position;
        this.anchor = new Vector(position.x(), position.y());
        this.sprite = sprite;
        this.bounds = new Rectangle((int)position.x(), (int)position.y(), GRID_SIZE, GRID_SIZE);
    }
    
    /**
     * Called every 60th of a second. Updates the Entity
     */
    public void tick(){
        sprite.tick();
    }
    
    /**
     * Removes an Entity from the game
     */
    public void destroy(){
        Controller.getInstance().remove(this);
    }
    
    /**
     * @return if the Entity is solid
     */
    public boolean isSolid(){
        return isSolid;
    }
    
    /**
     * @return current position of the Entity
     */
    public Vector getPosition() {
        return position;
    }

    /**
     * Sets the player's position
     * @param vector - position to move the player to
     */
    public void setPosition(Vector vector) {
        position = vector;
    }
    /**
     * @return the initial position of the Entity
     */
    public Vector getAnchor(){
        return anchor;
    }
    
    /**
     * @return the collision rectangle for the Entity
     */
    public Rectangle getBounds() {
        return bounds;
    }
    
    /**
     * @return The type of the Entity
     */
    public String getType(){ //TODO delete
        return type;
    }
    
    /**
     * @return The width of the Entity
     */
    public int getWidth() {
        return width;
    }
    
    /**
     * @return the height of the Entity
     */
    public int getHeight() {
        return height;
    }
    
    /**
     * @return the sprite of the Entity
     */
    public Sprite getSprite() {
        return sprite;
    }
    
    /**
     * Called when a key is pressed, if the entity is subscribed to updates
     * @param key the code of the key that was pressed
     */
    public void keyPressed(int key){
        
    }
    
    /**
     * Called when a key is released, if the entity is subscribed to updates
     * @param key the code of the key that was released
     */
    public void keyReleased(int key){
        
    }
}
