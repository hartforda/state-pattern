package Entity.Items;

import Engine.Vector;
import Entity.Entity;
import Graphics.Sprite;

/**
 * Item.java
 * Parent class of all collectibles
 * Created by hartforda on 2/8/2017.
 */
public abstract class Item extends Entity {
    
    /**
     * Constructor - creates the item
     * @param position position in the world
     * @param sprite the sprite of the item
     */
    public Item(Vector position, Sprite sprite){
        super(position, sprite);
        type = "item";
    }
}
