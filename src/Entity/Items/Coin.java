package Entity.Items;

import Engine.Vector;
import Graphics.Animation;
import Graphics.Assets;

/**
 * Coin.java
 * The player can collect coins to get points
 * Created by hartforda on 2/8/2017.
 */
public class Coin extends Item {
    
    /**
     * Constructor
     * @param position the position in the world
     */
    public Coin(Vector position){
        super(position, Assets.coin);

        isSolid = false;
        type = "coin";
    }
}
