package Engine;

import Entity.*;
import Entity.Hazards.Fire;
import Entity.Hazards.Spike;
import Entity.Items.Coin;
import Entity.Player.Player;

import java.io.*;
import java.util.Scanner;

/**
 * Utility class to read in a file os ASCII text and create a map from the characters
 * @author Connor Hibbs
 */
public class WorldGenerator {
    
    /** World size - the grid that components will be snapped to */
    private final int GRID_SIZE = Controller.GRID_SIZE;
    
    /** The controller that elements will be added to */
    private Controller controller;
    
    /** The file to load the world from */
    private File file;
    
    /**
     * Constructor - saves the file to load in from
     * @param file address of the file to load the world from
     */
    public WorldGenerator(String file){
        this.file = new File(file);
    }
    
    /**
     * Loads the world and creates the entities in the controller
     * @param controller the controller to add the entities to
     */
    public void loadWorld(Controller controller){
        this.controller = controller;
        
        try(Scanner scanner = new Scanner(file)){
            
            int y = 0;
            while(scanner.hasNextLine()){
                String line = scanner.nextLine();
                for(int x = 0; x < line.length(); x++){
                    handleChar(line.charAt(x), x, y);
                }
                y++;
            }
        } catch (IOException e){
            System.err.println("There was a problem generating the world");
        }
    }
    
    /**
     * Handles the character from the file, and creates an Entity
     * @param c character representation of the Entity
     * @param x x position (in grid)
     * @param y y position (in grid)
     */
    private void handleChar(char c, int x, int y){
        Entity e = getEntity(c, x, y);
        if(e == null) return;
        
        controller.subscribe(e);
    }
    
    /**
     * Creates an player at an absolute position
     * @param c character representation of the Entity
     * @param x x position (in grid)
     * @param y y position (in grid)
     * @return an Entity at absolute position
     */
    private Entity getEntity(char c, int x, int y){
        x *= GRID_SIZE;
        y *= GRID_SIZE;
        Vector position = new Vector(x, y);
        
        switch(Character.toLowerCase(c)){
            case '#': return new Wall(position);
            case '*': return new Player(position);
            case 'w': return new Fire(position);
            case '^': return new Spike(position);
            case 'o': return new Coin(position);
            default: return null;
        }
    }
}
