package Engine;

import Entity.Entity;

import java.awt.Rectangle;
import java.util.List;

/**
 * Collision detecting utility class
 * Note that an Entity should never be allowed to collide with itself
 * @author Ben Mahon original collision algorithm
 * @author Connor Hibbs expanded algorithm to functioning class
 */
public class Collisions {
    
    /**
     * Checks two rectangles to see if they are colliding
     * @param a rectangle a
     * @param b rectangle b
     * @return true if the rectangles are colliding
     */
    private static boolean checkRect(Rectangle a, Rectangle b){
        return !(b.x > a.x + a.width || b.y > a.y + a.height || b.x + b.width < a.x || b.y + b.height < a.y);
    }
    
    //////////////////////////////          Entity Collisions       ///////////////////////////
    
    /**
     * Checks an player for collisions with solid objects
     * @param self the player checking
     * @return the player that it is colliding with, or null if there are no collisions
     */
    public static Entity checkSolidCollision(Entity self){
        return checkSolidCollision(self, self.getBounds());
    }
    
    /**
     * Checks a rectangle for collision with solid objects
     * @param self this player is ignored in the map (self)
     * @param bounds the bounds being checked. If null, checks self's bounds
     * @return the player that it is colliding with, or null if there are no collisions
     */
    public static Entity checkSolidCollision(Entity self, Rectangle bounds){
        if(bounds == null) bounds = self.getBounds();
        List<Entity> entities = Controller.getInstance().getEntities();
        for(Entity e : entities){
            if(e == self) continue;
            if(e.isSolid() && checkRect(bounds, e.getBounds())){
                return e;
            }
        }
        return null;
    }
    
    /**
     * Checks an player for collisions against specific types of objects
     * @param self the player checking
     * @param type array of types to check against
     * @return the player that it is colliding with, or null if there are no collisions
     */
    public static Entity checkTypeCollision(Entity self, String... type){
        return checkTypeCollision(self, self.getBounds(), type);
    }
    
    /**
     * Checks an player for collisions against specific types of objects
     * @param self this player is ignored in the map (self)
     * @param bounds the actual rectangle being checked. If null, checks self's bounds
     * @param types array of types to check against
     * @return the player that it is colliding with, or null if there are no collisions
     */
    public static Entity checkTypeCollision(Entity self, Rectangle bounds, String... types){
        if(bounds == null) bounds = self.getBounds();
        List<Entity> entities = Controller.getInstance().getEntities();
        String type = self.getType();
        for(Entity e : entities){
            if(e == self) continue;
            for(int i = 0; i < types.length; i++){
                if(type.equals(types[i]) && checkRect(bounds, e.getBounds())){
                    return e;
                }
            }
        }
        return null;
    }
    
    /**
     * Checks an player for collision against entities of a specific class
     * @param self the player checking for collisions
     * @param against array of classes to check against
     * @return the player that it is colliding with, or null if there are no collisions
     */
    public static Entity checkClassCollision(Entity self, Class... against){
        return checkClassCollision(self, self.getBounds(), against);
    }
    
    /**
     * Checks an player for collision against entities of a specific class
     * @param self this player is ignored in the map (self)
     * @param bounds the actual bounds being checked for collision
     * @param against array of classes to check against
     * @return the player that it is colliding with, or null if there are no collisions
     */
    public static Entity checkClassCollision(Entity self, Rectangle bounds, Class... against){
        List<Entity> entities = Controller.getInstance().getEntities();
        for(Entity e : entities){
            if(e == self) continue;
            for(int i = 0; i < against.length; i++) {
                if (against[i].isInstance(e) && checkRect(bounds, e.getBounds())) {
                    return e;
                }
            }
        }
        return null;
    }
    
    ///////////////////        Boolean Collisions           ///////////////////////
    
    public static boolean checkCollision(Entity self){
        return checkSolidCollision(self) != null;
    }
    
    public static boolean checkCollision(Entity self, Rectangle bounds){
        return checkSolidCollision(self, bounds) != null;
    }
    
    public static boolean checkCollision(Entity self, String... types){
        return checkTypeCollision(self, types) != null;
    }
    
    public static boolean checkCollision(Entity self, Rectangle bounds, String... types){
        return checkTypeCollision(self, bounds, types) != null;
    }
    
    public static boolean checkCollision(Entity self, Class... against){
        return checkClassCollision(self, against) != null;
    }
    
    public static boolean checkCollision(Entity self, Rectangle bounds, Class... against){
        return checkClassCollision(self, bounds, against) != null;
    }
}
