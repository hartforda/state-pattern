package Engine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import Graphics.Assets;

/**
 * Main driver for the program. Creates the UI and starts the Controller
 * @author Connor Hibbs
 */
public class App extends JFrame {
    
    /** Width and height of the window */
    private final int WIDTH = 1000, HEIGHT = 750;
    
    /** The controller that runs the program */
    private Controller controller;
    
    /** The canvas that Entities are drawn to */
    private Canvas canvas;

    /** Reference to the app itself */
    private App app;

    private JPanel restart;

    /**
     * Constructor - creates the UI and drawing elements, then
     * creates and starts the controller (clock)
     */
    public App(){
        super("State Machine Demo");
        this.app = this;
        setBackground(Color.BLACK);
        canvas = new Canvas();
        canvas.requestFocus();

        add(canvas);

        setSize(WIDTH, HEIGHT);
//        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setVisible(true);
    
        //Create the world and start the clock
        controller = Controller.getInstance();

        generateWorld("bigWorld.txt");
        
        addKeyListener(KeyInput.getInstance());
        addComponentListener(resizeListener);
        addFocusListener(regainFocus);
    
        // This call must be the last thing in the constructor
        controller.start(this);
    }

    /**
     * Provides the user the ability to start a new game
     * @param winOrLoss - String containing "win" if the game was won, "loss" if the game was lost
     */
    public void playAgain(String winOrLoss) {

        System.out.println("Method is called: " + winOrLoss);
        JLabel label = null;
        restart = new JPanel();
        restart.setBackground(Color.white);

        if (winOrLoss.equals("win")) {
            restart.add(new JLabel(new ImageIcon(Assets.youWon)));
            label = new JLabel(new ImageIcon(Assets.playAgainW));
        }
        if (winOrLoss.equals("loss")) {
            restart.add(new JLabel(new ImageIcon(Assets.gameOver)));
            label = new JLabel(new ImageIcon(Assets.playAgainL));
        }

        assert label != null;
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.reset();
                controller = Controller.getInstance();
                generateWorld("bigWorld.txt");
                add(canvas);
                controller.start(app);
            }
        });
        remove(canvas);

        restart.add(label);
        this.add(restart);
        revalidate();
        controller.stop();
    }

    /**
     * Helper method to generate the game world
     * @param file - filepath to game world
     */
    private void generateWorld(String file) {
        WorldGenerator world = new WorldGenerator(file);
        world.loadWorld(controller);
    }
    
    /**
     * Main method
     * @param args ignored
     */
    public static void main(String args[]){
        App game = new App();
    }
    
    /**
     * @return drawing canvas
     */
    public Canvas getCanvas(){
        return canvas;
    }

    /**
     * ComponentListener that listens for window resize events, and notifies the controller that the center has changed
     */
    private ComponentListener resizeListener = new ComponentAdapter(){
        @Override
        public void componentResized(ComponentEvent e) {
            super.componentResized(e);
            controller.resizeRoom();
        }
    };
    
    /**
     * FocusListener which forces the main game window to remain in focus
     */
    private FocusListener regainFocus = new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
            super.focusLost(e);
            requestFocus();
        }
    };
}
