package Engine;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import Entity.Entity;

/**
 * Keeps track of keys' status. Supports all ASCII keys
 * @author Connor Hibbs
 */
public class KeyInput implements KeyListener{

    /** A mapping of which keys are pressed */
    private static boolean[] keys;
    
    private List<Entity> subscribers;
    
    private static KeyInput keyInput = new KeyInput();
    
    /**
     * Constructor - creates the key array
     */
    private KeyInput(){
        keys = new boolean[256];
        subscribers = new ArrayList<>();
    }
    
    public static KeyInput getInstance(){
        return keyInput;
    }
    
    @Override
    public void keyTyped(KeyEvent e) { }
    
    /**
     * Called on a key's press. Updates the value of the key in the array
     * and also notifies subscribers
     * @param e KeyEvent associated with the action
     */
    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if(keys[keyCode]) return; //do not continue if it was already pressed
        
        keys[keyCode] = true;
        for(Entity entity : subscribers){
            entity.keyPressed(keyCode);
        }
    }
    
    /**
     * Called of a key's release. Updates the value of the key in the array
     * and also notifies subscribers
     * @param e KeyEvent associated with the action
     */
    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        
        keys[keyCode] = false;
        for(Entity entity : subscribers){
            entity.keyReleased(keyCode);
        }
    }
    
    /** @return true if the up button or w is pressed */
    public static boolean up(){
        return keys[KeyEvent.VK_UP] || keys[KeyEvent.VK_W];
    }
    
    /** @return true if the down button or s is pressed */
    public static boolean down(){
        return keys[KeyEvent.VK_DOWN] || keys[KeyEvent.VK_S];
    }
    
    /** @return true if the right button or d is pressed */
    public static boolean right(){
        return keys[KeyEvent.VK_RIGHT] || keys[KeyEvent.VK_D];
    }
    
    /** @return true if the left button or a is pressed */
    public static boolean left(){
        return keys[KeyEvent.VK_LEFT] || keys[KeyEvent.VK_A];
    }
    
    /** @return true if the space button is pressed */
    public static boolean space() {
        return keys[KeyEvent.VK_SPACE];
    }
    
    
    
    public void subscribe(Entity entity){
        subscribers.add(entity);
    }
    
}