package Engine;

/**
 * @author Connor Hibbs
 */
public class Vector {
    
    private double x, y;
    
    public Vector(double x, double y){
        this.x = x;
        this.y = y;
    }
    
    public double x() {
        return x;
    }
    
    public double y() {
        return y;
    }
    
    public void x(double x) {
        this.x = x;
    }
    
    public void y(double y) {
        this.y = y;
    }
    
    public static Vector add(Vector a, Vector b){
        return new Vector(a.x() + b.x(), a.y() + b.y());
    }
    
    public static Vector sub(Vector a, Vector b){
        return new Vector(a.x() - b.x(), a.y() - b.y());
    }
    
    public String toString(){
        return "x: " + x + "\ty: " + y;
    }
}
