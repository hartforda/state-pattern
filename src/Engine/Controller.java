package Engine;

import Entity.Entity;
import Graphics.Assets;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.List;

/**
 * The controller of the game. Runs the primary clock thread. Ticks and renders all
 * of the entities
 * @author Connor Hibbs, hartforda
 */
public class Controller {
    
    /** Set flag to true to enable debugging output */
    final private boolean DEBUG = false;
    
    /** Grid size to draw entities to */
    public static final int GRID_SIZE = 32;
    
    /** The status of the controller */
    private boolean running;
    
    /** The primary clock thread */
    private Thread clock;
    
    /** A reference the the UI app */
    private App app;
    
    /** The center of the window where the following Entity should be drawn */
    private Vector center;

    /** Keeps track of player lives/coins */
    private int lifeCounter, coinCounter;
    
    /** The entities in the game */
    private List<Entity> entities;
    
    /** List of entities to be removed after each tick */
    private List<Entity> garbageCollector;
    
    /** The Entity the camera should follow */
    private Entity following;
    
    /** Private instance of the controller */
    private static Controller controller = new Controller();
    
    /**
     * Constructor - creates the list of entities, and initiates the images
     */
    private Controller(){
        running = false;
        
        entities = new ArrayList<>();
        garbageCollector = new ArrayList<>();
        Assets.init();
        lifeCounter = 3;
        coinCounter = 0;
    }
    
    /**
     * @return the Controller instance
     */
    public synchronized static Controller getInstance(){
        return controller;
    }
    
    /**
     * Adds an Entity to the list of items on screen that are updated every tick
     * @param entity The Entity to add
     */
    public void subscribe(Entity entity){
        entities.add(entity);
    }
    
    /**
     * Removes an Entity from the list of items on the screen
     * Effectively removes it from the program
     * @param entity Entity to remove
     */
    public void remove(Entity entity){
        garbageCollector.add(entity);
//        entities.remove(player);
    }
    
    /**
     * Starts the main thread
     * @param app the UI window the program is running in
     */
    public synchronized void start(App app) {
        if(running) return;
        running = true;
        
        this.app = app;
        center = new Vector(app.getWidth() / 2, app.getHeight() / 2);
        clock = new ClockThread();
        clock.start();
    }
    
    /**
     * Stops the main thread
     */
    public synchronized void stop(){
        if(!running) return;
        
        running = false;
//        try {
//            clock.join();
//        }catch (InterruptedException e){
//            e.printStackTrace();
//        }
    }

    /**
     * Makes a new controller to reset the game
     */
    public void reset() { controller = new Controller(); }
    
    /**
     * Called once every update
     */
    private void tick(){
        for (Entity e : entities) {
            e.tick();
        }
        for(Entity e : garbageCollector){
            entities.remove(e);
        }
        garbageCollector.clear();
    }

    /**
     * Helper method to render overlay
     */
    private void drawOverlay(Graphics2D g2d) {

        if(lifeCounter > 0){
            g2d.drawImage(Assets.lives[lifeCounter], 25, 25, null);
        } else {
            g2d.drawImage(Assets.lives[0], 25, 25, null);
            g2d.drawImage(Assets.gameOver, 100, 200, null);
            app.playAgain("loss");
            //g2d.drawImage(Assets.playAgainL, 175, 400, null);
        }
        
        if(coinCounter < 10){
            g2d.drawImage(Assets.coins[coinCounter], app.getWidth() - 360, 25, null);
        } else {
            g2d.drawImage(Assets.coins[10], app.getWidth() - 360, 25, null);
            g2d.drawImage(Assets.youWon, 155, 200, null);
            app.playAgain("win");
            //g2d.drawImage(Assets.playAgainW, 175, 400, null);
        }
    }

    /**
     * Updates the overlay image if the player loses a life, gains a coin, or won/lost the game
     * @param counter - String containing "coin" or "life" to determine which counter to increment/decrement
     */
    public void updateOverlay(String counter) {
        if (counter.equals("life")) lifeCounter--;
        if (counter.equals("coin")) coinCounter++;
    }
    
    /**
     * Renders the display and all elements in the display
     * Render Order:
     *      Background
     *      Entities
     *      Player
     *      Overlays
     */
    private void render(){
        //Create the BufferStrategy to make rendering smoother
        Canvas canvas = app.getCanvas();
        BufferStrategy bs = canvas.getBufferStrategy();
        if(bs==null){
            canvas.createBufferStrategy(2);
            bs = canvas.getBufferStrategy();
        }
        
        Graphics2D g2d = (Graphics2D) bs.getDrawGraphics();
        
        //Render all of the entities in the game
        g2d.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        Vector displacement = Vector.sub(following.getPosition(), center);
//        g2d.drawImage(Assets.background, -(int)displacement.x(), -(int)displacement.y(),null);
        g2d.drawImage(Assets.background, 0, 0, null);

        for(Entity e : entities){
            if(e == following) continue;
            
            Vector anchor = e.getAnchor();
            Vector pos = (following == null) ? anchor : Vector.sub(anchor, displacement);

            //TODO check if the player will even be seen
            g2d.drawImage(e.getSprite().getImage(), (int) pos.x(), (int) pos.y(), e.getWidth(), e.getHeight(), null);
        }

        drawOverlay(g2d); //make sure to draw overlays after the entities but before the player
        g2d.drawImage(following.getSprite().getImage(), (int)center.x(), (int)center.y(), following.getWidth(), following.getHeight(), null);
        
        g2d.dispose();
        bs.show();
    }
    
    /**
     * recalculates the center of the room where the following Entity should be drawn
     */
    public void resizeRoom(){
        center = new Vector(app.getWidth() / 2, app.getHeight() / 2);
    }
    
    /**
     * Sets the player for the camera to follow
     * @param entity Entity for the camera to follow
     */
    public void follow(Entity entity){
        following = entity;
    }
    
    /**
     * @return list of all entities on the amp
     */
    public List<Entity> getEntities() {
        return entities;
    }
    
    /**
     * Main thread which runs the clock and notifies all entities to tick() and render()
     */
    private class ClockThread extends Thread {
        
        long now, lastTime = System.nanoTime();
        final double amountOfTicks = 60.0;
        long ns = 1000000000;
        final double refreshRate = (1 / amountOfTicks) * ns;
        double delta = 0; //nanoseconds
        int updates = 0;
        int iterations = 0;
        long timer = System.currentTimeMillis();
        
        /**
         * Runs the main clock thread
         */
        @Override
        public void run() {
            
            while(running){
                now = System.nanoTime();
                delta += (now - lastTime);
                lastTime = now;
                
                if(delta >= refreshRate){
                    tick();
                    render();
                    updates++;
                    delta -= refreshRate;
                }
                iterations++;
                
                if(DEBUG && System.currentTimeMillis() - timer > 1000){
                    debug();
                }
            }
            System.out.println("Thread ran off");
        }
        
        /**
         * Prints out some debugging information about the speed of the program
         */
        private void debug(){
            timer = System.currentTimeMillis();
            System.out.println(updates + " Ticks, Iterations " + iterations);
            updates = 0;
            iterations = 0;
        }
    }
}
