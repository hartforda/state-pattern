package StackVisualizer;

import Entity.Player.StandingState;
import Entity.Player.State;

import javax.swing.*;
import java.awt.*;
import java.util.Stack;

/**
 * Simple demo GUI to show how the Pushdown Automata state stack grows and falls back down to a single state
 * @author Connor Hibbs
 */
public class StackApp extends JFrame {

    private int stackSize = 0;
    private JPanel content, stackPanel;
    private JLabel sizeLabel;
    private Stack<StateLabel> stack;

    public StackApp(){
        super("Stack Visualizer");
        
        content = new JPanel();
        content.setLayout(new BorderLayout());
        content.setBackground(Color.WHITE);
        
        stackPanel = new JPanel();
        stack = new Stack<>();

        sizeLabel = new JLabel("Stack Size: ");
        sizeLabel.setFont(new Font("Arial", Font.PLAIN, 20));
        
        setState(new StandingState(null));
        
        content.add(stackPanel, BorderLayout.CENTER);
        content.add(sizeLabel, BorderLayout.SOUTH);
        
        add(content);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(new Dimension(350, 750));
        setVisible(true);
    }

    public void setState(State state){
        StateLabel stateLabel = new StateLabel(state.toString());
        stackPanel.add(stateLabel);
        stack.push(stateLabel);
        
        stackSize++;
        updateDisplay();
    }
    
    public void prevState(){
        stackPanel.remove(stack.pop());
        stackSize--;
        updateDisplay();
    }
    
    public void updateDisplay(){
        revalidate();
        repaint();
        sizeLabel.setText("Stack Size: " + stackSize);
    }

    private class StateLabel extends JLabel {
        public StateLabel(String label){
            super(label);
            setFont(new Font("Arial", Font.BOLD, 32));
        }
    }
}
